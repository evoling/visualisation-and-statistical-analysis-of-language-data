# Visualisation and statistical analysis of language data

PhD course, 7.5hp

Level: Beginner (no prior experience assumed)

Examination: Regular practical exercises plus a small project

The aim of this course is to teach general principles and practice of carrying out sound quantitative humanities research. The course makes use of the statistical computer language R, which is increasingly becoming a standard tool in the human sciences, and which provides a generic way for developing a wide range of analysis types, from the most simple to the most complex. The course will give a non-mathematical introduction to the conceptual basis of some important types of statistical analysis, but the main focus is on teaching basic practical skills that will let you work towards becoming the quantitative researcher that you need to be in your PhD studies. As such, exploring and understanding your data through visualisation techniques may be more immediately useful than blindly applying statistical tests. Throughout the course we will be rehearsing the everyday research practices that contribute to making projects robust and reproducible, and of course, publishable. 

## 1	Introduction to RStudio

RStudio is a supercharged calculator, and also the centre of your quantitative world. This week we will focus on basic skills: Running RStudio, getting help, recording your work with markdown notebooks, writing and running scripts, reading and writing data files, and some exploration of basic data types.

## 2	Good enough practices in scientific computing

The software carpentry analogy: we are like domestic carpenters, not professional cabinetmakers. We can build a functional birdhouse, or a bookcase, but we do not aspire to make a fancy inlaid dresser. This week we will learn how to develop a reproducible, self-documenting workflow. We will also introduce using git (via RStudio) and GitHub for managing data and analysis.

## 3	Data structures and plotting

Basic R programming with data and introduction to the tidyverse, a family of R tools that  over the last few years have revolutionised how R is used

## 4	Data wrangling

A lot of our analytic work is what I call ‘data wrangling’: taking raw data and turning it into something that can be analysed. Often, once the data is in the right form, the analysis itself becomes easy. We will learn how to subset data, and other dataframe manipulations using dplyr. 

## 5	Data visualisation and exploration

Introduction to basic plot types, and the joys of faceted data.

## 6	Same or different?

At this point in the course we should be comfortable with working with data: reading, writing, transforming, and visualising. Now we will look at some statistical tests for telling whether values and distributions of values are the same or different. After going through some basic tests, we will focus on the process of discovering for yourself what you need to know in more complex cases.

## 7	Working with similarity measures

Similarity (the flip-side of difference) is an important concept in humanities computing. We will learn how to produce useful similarity measures, and how to analyse and visualise them using techniques such as Principal Components Analysis and Multidimensional Scaling

## 8	Data in the world

Data about humans and human behaviour often has a geographic aspect. We will learn how to plot data on geographic maps and learn some commonly used geostatistical tests.

## 9	What’s what?

An introduction to inferring the underlying order in your data through statistical classification

## 10	Putting it all together

Techniques for collaboration. Producing publication quality graphics. Archiving and publishing research analyses online (using e.g. FigShare). How to report your analysis in a thesis or paper. Other workflows (leaving RStudio for the text editor and command line) 

